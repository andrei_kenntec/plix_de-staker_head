#include <Servo.h>
#include <Math.h>

# define StepPulse_1 5
# define DirectionPulse_1 7
# define PowerDown_1 9
# define PulseOriginMonitor_1 11
# define Alarm_1 13

# define stackEndstop 2
# define conveyEndstop 3

# define fanPin  6
# define cartPin 10
# define headPin 8

# define headServUp 85
# define headServDown 30

# define wingServUp 65
# define wingServDown 110

# define stepperPulseSpeed 115

# define beckhoffSigOut 12
# define beckhoffSigIn A0

# define UP "up"
# define DOWN "down"

# define timeOutSec 180

Servo fanServ;
Servo headServ;
Servo wingServ;

int globalDelay1 = 10;
int timeOutCount = 0;


int wingServAngle;
int headServAngle;

int pos = 0;
int CurrentState = 0;
int inSig = 0;

volatile unsigned int motor0Pos = 3000;
volatile unsigned int motor1Pos = 3000;
int count = 0;

void moveToStack ();
void moveToConveyor();
void headMove(String dir, int moveAngle = 0);
void wingMove(String dir, int moveAngle = 0, int delayTime = 10);

void setup() {


  pinMode(StepPulse_1, OUTPUT);
  pinMode(DirectionPulse_1, OUTPUT);
  pinMode(PowerDown_1, OUTPUT);
  pinMode(beckhoffSigOut, OUTPUT);
  pinMode(beckhoffSigIn, INPUT);
  pinMode(PulseOriginMonitor_1, INPUT);
  pinMode(Alarm_1, INPUT);

  pinMode(stackEndstop, INPUT);
  pinMode(conveyEndstop, INPUT);

  digitalWrite(stackEndstop, HIGH);
  digitalWrite(conveyEndstop, HIGH);


  digitalWrite(PowerDown_1, HIGH);
  digitalWrite(StepPulse_1, LOW);

  fanServ.attach(fanPin);
  headServ.attach(cartPin);
  wingServ.attach(headPin);

  Serial.begin (9600);
}

void loop() {

  switch (CurrentState) {

    ////////////////////////////////////              SET UP        ////////////////////////
    case 0:
      //set fan controler on
      fanServ.write(55);
      delay(500);

      headMove(UP);

      wingMove(UP);

      moveToStack();

      CurrentState = 1; //change to 2 to run test code
      delay(2000);
      break;

    ////////////////////////////////////////////////       START        ///////////////////////////////////////////////////////////////

    case 1:

      //--------------------------------------------------Get wings into start position
      wingMove(DOWN, 23, 30);
      delay(500);
      //--------------------------------------------------Drop to stack
      headMove(DOWN);

      //--------------------------------------------------Sheldon Pickup
       fanServ.write(100);
       delay(500);
      
      //  Wings down
      wingMove(DOWN, 22, 10);
       
      // Wings Up
      wingMove(UP, 45, 10);                                                                                 

      //slow Fan
      fanServ.write(70);

      //Pull head up slightly
      headMove(UP, 8);
      
      //  Wings down
      wingMove(DOWN, 40, 10);

      // Wings Up
      wingMove(UP, 35, 30);

      
      fanServ.write(68);


      //--------------------------------------------------Raise Head
      headMove(UP);

      //--------------------------------------------------Shift to Conveyor
      moveToConveyor();

      //--------------------------------------------------Wait for Signal from Beckhoff. If no signal received in timeOutSec seconds then return to stack and wait for beckhoff signal
      timeOutCount = 0;
      while(!digitalRead(beckhoffSigIn)){
        timeOutCount++;
        if(timeOutCount == (timeOutSec*1000)){
          CurrentState = 4;
          break;
          
        }
        delay(1);
      }
      if(timeOutCount == timeOutSec*1000){
        break;
      }

      //--------------------------------------------------Lower Head
      fanServ.write(64); //fan low hold
      headMove(DOWN);

      //--------------------------------------------------Sheldon Drop
      delay(100);
      //raise wings to top
      wingMove(UP);
      fanServ.write(55);

      delay(1000);
      //-----------------------------------------------------Raise Head
      headMove(UP);

      //-----------------------------------------------------Send Signal to Beckhoff
      digitalWrite(beckhoffSigOut, HIGH);
      delay(100);
      digitalWrite(beckhoffSigOut, LOW);
      //-----------------------------------------------------Return To Stack
      moveToStack();

      break;

    //TEST STATE
    case 2:
      //(put code to test here)
      //run test code by changing CurrentState variable in setup code
      break;  
    
    //STOP STATE
    case 3:
      // DO NOTHING
      // Stops machine in place and turns off fan, 
      fanServ.write(55);
      break;

    //Return to Stack and Wait for signal
    case 4:
    
      fanServ.write(64); //fan hold

      //-----------------------------------------------------Raise Head
      headMove(UP);

     //------------------------------------------------------Return To Stack
      moveToStack();

      headMove(DOWN);
      fanServ.write(55);
      delay(500);
      headMove(UP);
      while(!digitalRead(beckhoffSigIn)){
        delay(1);
        CurrentState = 1;
      }
      break;

    default:
      // If for some reason State number doesn't match any options (something has gone wrong.
      CurrentState = 0;
      Serial.println("uh oh");
      break;
  }

  Serial.println(CurrentState);
  if (Serial.available() > 0) {
    inSig = Serial.read();
    //read from serial terminal. Enter 1, 8 or 9
    if (inSig = 1) {
      Serial.println(99);
      while (Serial.available() == 0) {
        delay(100);
      }
    } else if (inSig == 9) {
      //re run setup code
      CurrentState = 0;
    } else if (inSig == 8){
      //pause code and turn off fan
      CurrentState = 3;
    }
    Serial.flush();
  }
  Serial.flush();
}

void wingMove(String dir, int moveAngle, int delayTime) {
  if (dir == "down" && moveAngle == 0) {
    for (pos = wingServAngle; pos <= wingServDown; pos++) {
      wingServ.write(pos);
      //Serial.println(pos);
      delay(delayTime);
    }
    wingServAngle = pos;
    Serial.println("DOWN!");
  }
  else if (dir == "up" && moveAngle == 0) {
    for (pos = wingServAngle; pos >= wingServUp; pos--) {
      wingServ.write(pos);
      //Serial.println(pos);
      delay(delayTime);
    }
    wingServAngle = pos;
    Serial.println("NOT DOWN!");
  }
  else {
    if(dir == "down"){
      for (pos = wingServAngle; pos <= wingServAngle + moveAngle; pos++) {
        if(pos > wingServDown){
          break;
        }
        wingServ.write(pos);
        //Serial.println(pos);
        delay(delayTime);
      }
      wingServAngle = pos;
    }else if(dir == "up"){
      for (pos = wingServAngle; pos >= wingServAngle - moveAngle; pos--) {
        if(pos < wingServUp){
          Serial.println("broke?");
          break;
        }
        wingServ.write(pos);
        //Serial.println(pos);
        delay(delayTime);
      }
      wingServAngle = pos;
    }
    Serial.println(dir);
  }
  Serial.println(wingServAngle);
}

void headMove(String dir, int moveAngle) {
  if (dir == "up" && moveAngle == 0) {
    for (pos = headServAngle; pos <= headServUp; pos++) {
      headServ.write(pos);
     // Serial.println(pos);
      delay(10);
    }
    headServAngle = pos;
    //Serial.println("UP!");
  }
  else if (dir == "down" && moveAngle == 0) {
    for (pos = headServAngle; pos >= headServDown; pos--) {
      headServ.write(pos);
      //Serial.println(pos);
      delay(5);
    }
    headServAngle = pos;
    //Serial.println("NOT UP!");
  }
  else {
    if(dir == "up"){
      for (pos = headServAngle; pos <= headServAngle + moveAngle; pos++) {
        headServ.write(pos);
        //Serial.println(pos);
        delay(10);
      }
      headServAngle = pos;
    }else if(dir == "down"){
      for (pos = headServAngle; pos >= headServAngle - moveAngle; pos--) {
        headServ.write(pos);
        //Serial.println(pos);
        delay(10);
      }
      headServAngle = pos;
    }
    //Serial.println(dir);
  }

}

void  moveToStack() {
  digitalWrite(DirectionPulse_1, HIGH);
  int counter = 0;
  float counterD = 0.0;
  float counterF = 0.0;
  signed int rampDelay = 0;
  while (digitalRead(stackEndstop)){
    counter++;
    //run if statement every 1000 counts
    if(counter%1000 == 0){
      //this dumb line is just to change the data type to float from int
      counterF = counter;
      //Create a cos wave to speed up and slow down the stepper motor over 15000 steps
      counterD = (counterF/15000)*6.283;
      rampDelay = 25 * cos(counterD); 
      Serial.println(rampDelay); 
    }
    digitalWrite(StepPulse_1, LOW);
    delayMicroseconds(stepperPulseSpeed + rampDelay);
    digitalWrite(StepPulse_1, HIGH);
    delayMicroseconds(stepperPulseSpeed + rampDelay);
  }
  Serial.println(counter);
  return;
}

void moveToConveyor() {
  digitalWrite(DirectionPulse_1, LOW);
  int counter = 0;
  float counterF = 0.0;
  float counterD = 0.0;
  signed int rampDelay = 0;
  while (digitalRead(conveyEndstop)) {
    counter++;
    //run if statement every 1000 counts
    if(counter%1000 == 0){
      //this dumb line is just to change the data type to float from int
      counterF = counter;
      //Create a cos wave to speed up and slow down the stepper motor over 15000 steps
      counterD = (counterF/15000)*6.283;
      rampDelay = 25 * cos(counterD);
      Serial.println(counterD);
    }
    digitalWrite(StepPulse_1, LOW);
    delayMicroseconds(stepperPulseSpeed + rampDelay);
    digitalWrite(StepPulse_1, HIGH);
    delayMicroseconds(stepperPulseSpeed + rampDelay);
  }
  Serial.println(counter);
  return;
}

