// Test motor joint mode

#include "DynamixelMotor.h"
#include <LiquidCrystal.h>
#include <Servo.h>

//servo numbesr
Servo ServoNumberFan;                                                    ///////////////////////////////
Servo ServoNumberUpDn;                                                      /////////////////////////////

// software serial pins, if you use software serial
#define SOFT_RX_PIN 2
#define SOFT_TX_PIN 3

const uint8_t M1id = 1; // Set up motor IDs here
const uint8_t M2id = 2;

// select the pins used on the LCD panel
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);
// define some values used by the panel and buttons
int pos = 0;                                                                /////////////////////
int pos1 = 50;                                                              /////////////////////
int pos2 = 64;                                                              /////////////////////
int pos3 = 50;                                                              /////////////////////
int pos4 = 50;

int posVert = 64;
int posFan = 0;

int lcd_key     = 0;
int adc_key_in  = 0;

#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5

int motor1speed;
int motor2speed;
int motor1target;
int motor2target;

// communication baudrate
const long unsigned int baudrate = 9615;

// hardware serial without tristate buffer
// see blink_led example, and adapt to your configuration
HardwareDynamixelInterface interface(Serial);

DynamixelMotor motor1(interface, M1id);
DynamixelMotor motor2(interface, M2id);

uint8_t led_state = true;

int CurrentState = 0;

// read the buttons
int read_LCD_buttons()
{
  adc_key_in = analogRead(0);      // read the value from the sensor
  // my buttons when read are centered at these valies: 0, 144, 329, 504, 741
  // we add approx 50 to those values and check to see if we are close
  if (adc_key_in > 1000) return btnNONE; // We make this the 1st option for speed reasons since it will be the most likely result
  // For V1.1 us this threshold
  if (adc_key_in < 50)   return btnRIGHT;
  if (adc_key_in < 250)  return btnUP;
  if (adc_key_in < 450)  return btnDOWN;
  if (adc_key_in < 650)  return btnLEFT;
  if (adc_key_in < 850)  return btnSELECT;


  return btnNONE;  // when all others fail, return this...
}



void motor1setposition(int position, int speed) {

  motor1.speed(speed);
  motor1.goalPosition(position);
  motor1target = position;
}

void motor2setposition(int position, int speed) {

  motor2.speed(speed);
  motor2.goalPosition(position);
  motor2target = position;
}


bool motor1inposition()
{
  if ( abs(motor1.currentPosition() - motor1target) <= 8) {
    return HIGH;
  } else {
    return LOW;
  }
}

bool motor2inposition()
{
  if ( abs(motor2.currentPosition() - motor2target) <= 8) {
    return HIGH;
  } else {
    return LOW;
  }
}
//functions
void FanCalabrate();                                              /////////////////////
void FanIdle();                                                   /////////////////////
void FanHold();                                                   /////////////////////
void FanMid();                                                    /////////////////////
void FanHigh();                                                   /////////////////////

void VertDown();
void VertTouch();
void Vert10mm();
void VertMid();
void VertMaxUp();

void WingCleanDownFromFlat();                                     /////////////////////
void WingCleanUpFromFlat();                                       /////////////////////

void WingDown();                                         /////////////////////
void WingFlat();                                         /////////////////////
void WingUp();                                           /////////////////////
void WingHalfDown();


void setup()
{
  // pinMode(11, OUTPUT);

  ServoNumberFan.write(pos1);                                     /////////////////////
  ServoNumberUpDn.write(posVert);                                    /////////////////////
  ServoNumberFan.attach(11);                                      /////////////////////
  ServoNumberUpDn.attach(12);                                     /////////////////////

  //Setup LCD
  lcd.begin(16, 2);              // start the library
  lcd.setCursor(0, 0);

  //setup motors
  interface.begin(baudrate);
  delay(100);

  ///Serial.begin(9600);

  // check if we can communicate with the motor
  // if not, we turn the led on and stop here
  uint8_t status = motor1.init();
  if (status != DYN_STATUS_OK)
  {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
    lcd.setCursor(0, 0);
    lcd.print("M1 not talking");
    while (1);
  }

  uint8_t status1 = motor2.init();
  if (status1 != DYN_STATUS_OK)
  {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
    lcd.setCursor(0, 1);
    lcd.print("M2 not talking");
    while (1);
  }


  motor1.enableTorque();
  motor2.enableTorque();

  // set to joint mode, with a 180° angle range
  // see robotis doc to compute angle values
  motor1.jointMode(204, 820);
  motor2.jointMode(204, 820);
  
  FanCalabrate();
  VertMid();
  WingFlat();
}

void loop()

{
  lcd.setCursor(14, 0);
  lcd.print(CurrentState);

  switch (CurrentState) {
    /////////////////////////

    case 0:
      //Wait for button
      lcd.setCursor(0, 0);
      lcd.print("Wait Button");
      if(read_LCD_buttons() == btnSELECT){
        CurrentState = 100;
      }
    break;

    case 100:
      //Go to pickup
      Vert10mm();
      FanHigh();
      delay(700);
      CurrentState = 200;
    break;
    
    case 200:
      //Start seperate sequence
      VertMid();
      WingUp();
      if ( motor1inposition() &&  motor2inposition()) { /////////////this if checks if wing movement is done
        CurrentState = 300;
      }
    break;

   case 300:
      FanMid();
      WingHalfDown();
      if ( motor1inposition() &&  motor2inposition()) { /////////////this if checks if wing movement is done
        CurrentState = 400;
      } 
   break;

   case 400:
         WingUp();
      if ( motor1inposition() &&  motor2inposition()) { /////////////this if checks if wing movement is done
        CurrentState = 500;
      }
    break;

   case 500:
      WingHalfDown();
      if ( motor1inposition() &&  motor2inposition()) { /////////////this if checks if wing movement is done
        CurrentState = 600;
      } 
   break;

   case 600:
      //Finish Seperating
      WingFlat();
      if ( motor1inposition() &&  motor2inposition()) { /////////////this if checks if wing movement is done
        CurrentState = 700;
      } 
   break;

   case 700:
    VertMaxUp();
    FanHold();
    CurrentState = 800;
   break;

   case 800:
     //Wait for button so can check plix pickup
      lcd.setCursor(0, 0);
      lcd.print("Wait Button");
     if(read_LCD_buttons() == btnSELECT){
       CurrentState = 900;
     }
   break;

   case 900:
     FanIdle();
     CurrentState = 0;
   break;

    
}
}

////////////////////////////// function's  //////////////////////////
int servoMove(int angle1, int angle2, Servo servoNumber, int servoSpeed) {

  if (angle1 >= angle2) {
    for (pos = angle1 ; pos >= angle2  ; pos -= 1 )
    { servoNumber .write(pos);
      delay(  servoSpeed );
    }
  }

  if (angle1 <= angle2) {
    for (pos = angle1 ; pos <= angle2  ; pos += 1 )
    { servoNumber .write(pos);
      delay(  servoSpeed );
    }
  }
  return angle2;
}

void FanCalabrate() {
  posFan = 48;
  ServoNumberFan.write(48);
  lcd.setCursor(0, 0);
  lcd.print("Fan cal");
  delay(6000);
}
void FanIdle() {
   posFan = servoMove(posFan , 51,ServoNumberFan , 30 );
   lcd.setCursor(0, 0);
   lcd.print("      Fan idle");
  //  delay(5000);

}


void FanHold() {
   posFan = servoMove(posFan , 55,ServoNumberFan , 30 );
   lcd.setCursor(0, 0);
   lcd.print("      Fan Hold");
  //  delay(5000);

}                                                  /////////////////////
void FanMid() {
   posFan = servoMove(posFan , 66,ServoNumberFan , 30 );
   lcd.setCursor(0, 0);
   lcd.print("      Fan Mid");
  //  delay(5000);

}                                                    /////////////////////
void FanHigh() {
   posFan = servoMove(posFan , 72,ServoNumberFan , 30 );
   lcd.setCursor(0, 0);
   lcd.print("      Fan idle");
  //  delay(5000);

}                                                   /////////////////////


void VertDown(){
        lcd.setCursor(0, 0);
      lcd.print("Vert Down");
  posVert = servoMove(posVert , 30,ServoNumberUpDn , 15 );
}
void VertTouch(){
        lcd.setCursor(0, 0);
      lcd.print("Vert Touch");
  posVert = servoMove(posVert , 69,ServoNumberUpDn , 15 );
}
void Vert10mm(){
      lcd.setCursor(0, 0);
      lcd.print("Vert 10mm");
  posVert = servoMove(posVert , 77,ServoNumberUpDn , 15 );
}
void VertMid(){
          lcd.setCursor(0, 0);
      lcd.print("Vert Mid");
  posVert = servoMove(posVert , 90,ServoNumberUpDn , 30 );
}
void VertMaxUp(){
           lcd.setCursor(0, 0);
      lcd.print("Vert Max Up");
  posVert = servoMove(posVert , 140,ServoNumberUpDn , 15 );
}

void WingDown() {
      lcd.setCursor(0, 0);
      lcd.print("Wings Down");/////////////////////
  motor1setposition(480, 50);
  motor2setposition(480, 50);
}
void WingFlat(){/////////////////////
        lcd.setCursor(0, 0);
      lcd.print("Wings Flat");
  motor1setposition(575, 50);
  motor2setposition(565, 50); 
}
void WingUp(){
      lcd.setCursor(0, 0);
      lcd.print("Wings Up");
  motor1setposition(600, 80);
  motor2setposition(600, 80); 
}

void WingHalfDown(){
      lcd.setCursor(0, 0);
      lcd.print("Wings HaDown");
  motor1setposition(525, 80);
  motor2setposition(525, 80); 
}
